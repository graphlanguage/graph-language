# Graph Language

## Prerequisites

Install the following softwares before compilation and execution:

* [GIT](https://git-scm.com/) Version Control
* [Java 8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) Development kit + Runtime
* [Maven 3.5.x](https://maven.apache.org/) Build and dependency management
* [Eclipse IDE](https://www.eclipse.org/downloads/) (Optional)

## Compilation

Compile using maven (also executes the tests in the same time). Make sure you have the right proxies for maven to fetch the dependencies.

`mvn clean package`

## Run

Execute from target folder (or simply double-click on the jar file using file browser):

`java -jar graph-language-0.0.1-SNAPSHOT-jar-with-dependencies.jar`
