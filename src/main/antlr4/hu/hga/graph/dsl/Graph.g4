grammar Graph;

graphList:
	graph ('.'? graph)*;


graph:
	VERTEX ('-'? VERTEX)*;

VERTEX: ('A'..'Z');
