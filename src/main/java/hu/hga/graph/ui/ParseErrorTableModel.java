package hu.hga.graph.ui;

import java.util.Collections;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import hu.hga.graph.dsl.ParseError;

class ParseErrorTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 1L;
    private static final String[] COLUMN_NAME = { "Error message", "Detail", "Offset", "Length" };
    private List<ParseError> errors = Collections.emptyList();

    ParseErrorTableModel(List<ParseError> errors) {
        this.errors = errors;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnIndex < 2 ? String.class : Integer.class;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAME[columnIndex];
    }

    public ParseError getRow(int rowIndex) {
        return errors.get(rowIndex);
    }

    @Override
    public int getRowCount() {
        return errors == null ? 0 : errors.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object ret = null;
        ParseError error = errors.get(rowIndex);
        switch (columnIndex) {
            case 0:
                ret = error.getMessage();
                break;
            case 1:
                ret = error.getSymbol();
                break;
            case 2:
                ret = error.getBegin();
                break;
            case 3:
                ret = error.getLength();
                break;
        }
        return ret;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        // NOP
    }
}
