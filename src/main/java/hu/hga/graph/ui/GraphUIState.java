package hu.hga.graph.ui;

enum GraphUIState {
    INPUT,
    ERROR,
    SUCCESS;

    private static GraphUIState currentState = INPUT;

    private static boolean confirmState(GraphUIState state) {
        return currentState == state;
    }

    private static boolean isStateChanges(GraphUIState newState) {
        return currentState != newState;
    }

    private static void moveFromInputState(GraphUIState newState) {
        if (isStateChanges(newState)) {
            if (currentState == INPUT) {
                currentState = newState;
            } else {
                throw new IllegalStateException("Illegal state change.");
            }
        }
    }

    static void error() {
        moveFromInputState(ERROR);
    }

    static void input() {
        currentState = INPUT;
    }

    static boolean isError() {
        return confirmState(ERROR);
    }

    static boolean isInput() {
        return confirmState(INPUT);
    }

    static boolean isSuccess() {
        return confirmState(SUCCESS);
    }

    static void success() {
        moveFromInputState(SUCCESS);
    }
}
