package hu.hga.graph.ui;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

class InlineDocumentFilter extends DocumentFilter {

    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
        string = removeEnter(string);
        super.insertString(fb, offset, string, attr);
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
        text = removeEnter(text);
        super.replace(fb, offset, length, text, attrs);
    }

    private String removeEnter(String string) {
        return string.replaceAll("\n", "");
    }
}
