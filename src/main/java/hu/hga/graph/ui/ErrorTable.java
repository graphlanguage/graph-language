package hu.hga.graph.ui;

import java.awt.Color;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import hu.hga.graph.dsl.ParseError;

class ErrorTable extends JTable {

    private static final long serialVersionUID = 1L;
    private final ListSelectionListener selectionListener;
    private final ListSelectionModel selectionModel = new DefaultListSelectionModel();

    ErrorTable(ListSelectionListener selectionListener) {
        this.selectionListener = selectionListener;
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setSelectionModel(selectionModel);
        setForeground(Color.RED);
        getTableHeader().setForeground(Color.RED);
    }

    void removeListSelectionListener() {
        selectionModel.removeListSelectionListener(selectionListener);
    }

    @Override
    public void setModel(TableModel dataModel) {
        super.setModel(dataModel);
        if (selectionModel != null) {
            selectionModel.addListSelectionListener(selectionListener);
            selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }
    }

    ParseError getSelectedError() {
        int selectedRow = getSelectedRow();
        return selectedRow > -1 ? ((ParseErrorTableModel) dataModel).getRow(selectedRow) : null;
    }
}
