package hu.hga.graph.ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import hu.hga.graph.dsl.GraphLanguageParser;
import hu.hga.graph.dsl.ParseError;

public class GraphUI extends JFrame implements ListSelectionListener {

    private static final String SPLASH_MESSAGE = //
            "Type your input into the text field and press enter.\n" //
            + "Successful parsing results in green human readable form. \n" //
            + "An error browser is offered to fix the input.";

    private static final long serialVersionUID = 1L;
    private static final String OUTPUT_EMPTY = "Empty";
    private static final String OUTPUT_ERROR = "Error";
    private static final String OUTPUT_SUCCESS = "Success";
    private static final String TITLE = "Graph UI";
    private static final SimpleAttributeSet STYLE_ERROR = new SimpleAttributeSet();
    private static final SimpleAttributeSet STYLE_INPUT = new SimpleAttributeSet();
    private static final Set<Integer> NAVIGATION_KEY_CODES = new HashSet<>();
    static {
        NAVIGATION_KEY_CODES.add(KeyEvent.VK_UP);
        NAVIGATION_KEY_CODES.add(KeyEvent.VK_DOWN);
        NAVIGATION_KEY_CODES.add(KeyEvent.VK_LEFT);
        NAVIGATION_KEY_CODES.add(KeyEvent.VK_RIGHT);
        NAVIGATION_KEY_CODES.add(KeyEvent.VK_HOME);
        NAVIGATION_KEY_CODES.add(KeyEvent.VK_END);
        StyleConstants.setBackground(STYLE_ERROR, new Color(255, 200, 200));
        StyleConstants.setForeground(STYLE_ERROR, Color.BLACK);
        StyleConstants.setBackground(STYLE_INPUT, Color.WHITE);
        StyleConstants.setForeground(STYLE_INPUT, Color.BLACK);
    }

    public static void main(String[] args) {
        GraphUI ui = new GraphUI();
        ui.setVisible(true);
        JOptionPane.showMessageDialog(ui, SPLASH_MESSAGE);
    }

    private StyledDocument inputDocument = new DefaultStyledDocument();
    private JPanel outputCardPanel = new JPanel();
    private CardLayout outputCardLayout = new CardLayout();
    private JTextArea outputText = new JTextArea();
    private ErrorTable errorTable = new ErrorTable(this);

    private GraphUI() throws HeadlessException {
        super(TITLE);
        setSize(800, 300);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        getRootPane().setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        createInputComponent();
        createOutputComponent();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        ParseError error = errorTable.getSelectedError();
        if (error != null) {
            applyInputStyle(STYLE_INPUT);
            applyInputStyle(STYLE_ERROR, error.getBegin(), error.getLength());
        }
    }

    private void applyInputStyle(SimpleAttributeSet attr) {
        applyInputStyle(attr, 0, inputDocument.getLength());
    }

    private void applyInputStyle(SimpleAttributeSet attr, int start, int length) {
        inputDocument.setCharacterAttributes(start, length, attr, true);
    }

    private void createInputComponent() {
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BorderLayout());
        inputPanel.setBackground(Color.WHITE);
        inputPanel.add(new JLabel("Input:"), BorderLayout.WEST);
        applyInputStyle(STYLE_INPUT);
        ((AbstractDocument) inputDocument).setDocumentFilter(new InlineDocumentFilter());
        JTextPane textPane = new JTextPane(inputDocument);
        textPane.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent event) {
                if (!GraphUIState.isInput() && !NAVIGATION_KEY_CODES.contains(event.getKeyCode()) || inputDocument.getLength() == 0) {
                    GraphUIState.input();
                    applyInputStyle(STYLE_INPUT);
                    errorTable.removeListSelectionListener();
                    showOutputComponent(OUTPUT_EMPTY);
                }
                if (event.getKeyCode() == KeyEvent.VK_ENTER) {
                    GraphLanguageParser parser = new GraphLanguageParser();
                    parser.parse(textPane.getText());
                    displayParserResult(parser);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        inputPanel.add(textPane, BorderLayout.CENTER);
        add(inputPanel, BorderLayout.NORTH);
    }

    private void createOutputComponent() {
        outputText.setForeground(new Color(0, 200, 0));
        outputCardPanel.setLayout(outputCardLayout);
        outputCardPanel.add(new JScrollPane(outputText), OUTPUT_SUCCESS);
        outputCardPanel.add(new JScrollPane(errorTable), OUTPUT_ERROR);
        outputCardPanel.add(new JPanel(), OUTPUT_EMPTY);
        add(outputCardPanel, BorderLayout.CENTER);
    }

    private void displayParserResult(GraphLanguageParser parser) {
        List<ParseError> errors = parser.getErrors();
        if (errors.isEmpty()) {
            GraphUIState.success();
            outputText.setText(parser.getHumanReadableGraphDescription());
            showOutputComponent(OUTPUT_SUCCESS);
        } else {
            GraphUIState.error();
            errorTable.setModel(new ParseErrorTableModel(errors));
            errors.forEach(e -> applyInputStyle(STYLE_ERROR, e.getBegin(), e.getLength()));
            showOutputComponent(OUTPUT_ERROR);
        }
    }

    private void showOutputComponent(String component) {
        outputCardLayout.show(outputCardPanel, component);
    }
}
