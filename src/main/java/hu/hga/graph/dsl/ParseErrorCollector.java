package hu.hga.graph.dsl;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

class ParseErrorCollector extends BaseErrorListener {

    private final List<ParseError> errors = new ArrayList<>();

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        errors.add(new ParseError(charPositionInLine, 1, msg, offendingSymbol));
    }

    List<ParseError> getErrors() {
        return errors;
    }

    void validationError(String msg) {
        errors.add(new ParseError(0, 0, msg, null));
    }

    void validationError(String msg, ParserRuleContext context) {
        int start = context.getStart().getCharPositionInLine();
        int len = context.getStop().getCharPositionInLine() - start + 1;
        errors.add(new ParseError(start, len, msg, context.getText()));
    }
}
