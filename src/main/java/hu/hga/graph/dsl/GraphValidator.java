package hu.hga.graph.dsl;

import java.util.Set;

import org.antlr.v4.runtime.tree.TerminalNode;

import hu.hga.graph.dsl.GraphParser.GraphContext;
import hu.hga.graph.model.Graph;
import hu.hga.graph.model.GraphException;

class GraphValidator extends GraphBaseListener {

    private final ParseErrorCollector errorCollector;
    private final GraphCollector graphCollector;

    GraphValidator(ParseErrorCollector errorCollector, GraphCollector graphCollector) {
        this.errorCollector = errorCollector;
        this.graphCollector = graphCollector;
    }

    @Override
    public void exitGraph(GraphContext ctx) {
        Graph graph = validateAndBuildGraph(ctx);
        validateIntersection(graph, ctx);
        graphCollector.add(graph);
    }

    private char getVertexAsChar(TerminalNode lastTerminal) {
        return lastTerminal.getText().charAt(0);
    }

    private Graph validateAndBuildGraph(GraphContext ctx) {
        Graph graph = new Graph();
        for (TerminalNode currentTerminal : ctx.VERTEX()) {
            try {
                char vertex = getVertexAsChar(currentTerminal);
                graph.addVertex(vertex);
            } catch (GraphException ge) {
                errorCollector.validationError(ge.getMessage(), ctx);
            }
        }
        return graph;
    }

    private void validateIntersection(Graph graph, GraphContext ctx) {
        for (Graph g : graphCollector) {
            Set<Character> overlappingVertices = g.getOverlappingVertices(graph);
            if (!overlappingVertices.isEmpty()) {
                errorCollector.validationError("Vertex/vertices reused: " + overlappingVertices, ctx);
            }
        }
    }
}
