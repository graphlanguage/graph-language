package hu.hga.graph.dsl;

import java.util.List;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import hu.hga.graph.model.Graph;

public class GraphLanguageParser {

    private final ParseErrorCollector errorCollector = new ParseErrorCollector();
    private final GraphCollector graphCollector = new GraphCollector();

    public List<ParseError> getErrors() {
        return errorCollector.getErrors();
    }

    public List<Graph> getGraphs() {
        return graphCollector;
    }

    public String getHumanReadableGraphDescription() {
        StringBuilder ret = new StringBuilder();
        if (getErrors().isEmpty()) {
            List<Graph> graphs = getGraphs();
            ret.append(String.format("The input contains %d graph(s).\n", graphs.size()));
            int idx = 1;
            for (Graph g : graphs) {
                ret.append(String.format("The %d. graph contains: %s\n", idx, g.getDistinctVertices()));
                if (g.getEdges().isEmpty()) {
                    ret.append(String.format("The %d. graph has no edges.\n", idx));
                } else {
                    ret.append(String.format("The edges in %d. graph are: %s\n", idx, g.getEdges()));
                }
                idx++;
            }
        } else {
            ret.append(String.format("The input contains %d error(s):\n", getErrors().size()));
            getErrors().forEach(e -> ret.append(e.getMessage() + '\n'));
        }
        return ret.toString();
    }

    public void parse(String inputText) {
        if (inputText == null || inputText.isEmpty()) {
            errorCollector.validationError("No graph defined.");
        } else {
            CharStream input = CharStreams.fromString(inputText);
            GraphLexer lexer = new GraphLexer(input);
            GraphParser parser = new GraphParser(new CommonTokenStream(lexer));
            lexer.removeErrorListeners();
            lexer.addErrorListener(errorCollector);
            parser.removeErrorListeners();
            parser.addErrorListener(errorCollector);
            parser.addParseListener(new GraphValidator(errorCollector, graphCollector));
            parser.graphList();
        }
    }
}
