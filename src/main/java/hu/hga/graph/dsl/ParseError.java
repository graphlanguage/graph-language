package hu.hga.graph.dsl;

public class ParseError {

    private final int begin;
    private final int length;
    private final String message;
    private final Object symbol;

    public ParseError(int begin, int length, String message, Object symbol) {
        this.begin = begin;
        this.length = length;
        this.message = message;
        this.symbol = symbol;
    }

    public int getBegin() {
        return begin;
    }

    public int getLength() {
        return length;
    }

    public String getMessage() {
        return message;
    }

    public Object getSymbol() {
        return symbol;
    }
}
