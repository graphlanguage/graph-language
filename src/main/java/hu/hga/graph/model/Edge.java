package hu.hga.graph.model;

public class Edge {

    private final char vertex1;
    private final char vertex2;

    public Edge(char vertex1, char vertex2) {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        } else if (other instanceof Edge) {
            Edge otherEdge = (Edge) other;
            return getNormalizedSource() == otherEdge.getNormalizedSource() && getNormalizedTarget() == otherEdge.getNormalizedTarget();
        } else {
            return false;
        }
    }

    public char getVertex1() {
        return vertex1;
    }

    public char getVertex2() {
        return vertex2;
    }

    @Override
    public int hashCode() {
        return getNormalizedSource();
    }

    @Override
    public String toString() {
        return vertex1 + "-" + vertex2;
    }

    private boolean areVerticesInAlphaOrder() {
        return vertex1 < vertex2;
    }

    private char getNormalizedSource() {
        return areVerticesInAlphaOrder() ? vertex1 : vertex2;
    }

    private char getNormalizedTarget() {
        return areVerticesInAlphaOrder() ? vertex2 : vertex1;
    }
}
