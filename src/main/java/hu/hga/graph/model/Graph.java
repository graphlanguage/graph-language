package hu.hga.graph.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Graph {

    private final List<Character> vertices = new ArrayList<>();
    private final Set<Edge> edges = new LinkedHashSet<>();

    public void addVertex(char vertex) throws GraphException {
        vertices.add(vertex);
        if (vertices.size() > 1) {
            char lastVertex = vertices.get(vertices.size() - 2);
            if (vertex == lastVertex) {
                throw new GraphException("Vertex connected to self: " + vertex);
            } else {
                addEdge(new Edge(lastVertex, vertex));
            }
        }
    }

    public Set<Character> getDistinctVertices() {
        return new LinkedHashSet<>(vertices);
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    public List<Character> getVertices() {
        return vertices;
    }

    public Set<Character> getOverlappingVertices(Graph graph) {
        Set<Character> overlappingVertices = new HashSet<>(vertices);
        overlappingVertices.retainAll(graph.getVertices());
        return overlappingVertices;
    }

    private void addEdge(Edge edge) throws GraphException {
        if (!edges.add(edge)) {
            throw new GraphException("Edge already exists: " + edge);
        }
    }
}
