package hu.hga.graph.dsl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class GraphLanguageTest {

    @Test
    public void testHomeworkExample() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("ABC-E.H-IJH");
        List<ParseError> errors = parser.getErrors();
        Assert.assertTrue(errors.isEmpty());
        Assert.assertEquals(2, parser.getGraphs().size());
        Assert.assertEquals("The input contains 2 graph(s).\n" + 
                "The 1. graph contains: [A, B, C, E]\n" + 
                "The edges in 1. graph are: [A-B, B-C, C-E]\n" + 
                "The 2. graph contains: [H, I, J]\n" + 
                "The edges in 2. graph are: [H-I, I-J, J-H]\n", parser.getHumanReadableGraphDescription());
    }

    @Test
    public void testDoubleMinus() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("AB--C");
        List<ParseError> errors = parser.getErrors();
        Assert.assertEquals(1, errors.size());
        ParseError error = errors.get(0);
        Assert.assertEquals("extraneous input '-' expecting VERTEX", error.getMessage());
        Assert.assertEquals(3, error.getBegin());
    }

    @Test
    public void testEmptyInput() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("");
        List<ParseError> errors = parser.getErrors();
        Assert.assertFalse(errors.isEmpty());
        Assert.assertEquals("No graph defined.", errors.get(0).getMessage());
        Assert.assertTrue(parser.getGraphs().isEmpty());
    }

    @Test
    public void testGraphIntersection() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("ABC.AB.C");
        List<ParseError> errors = parser.getErrors();
        Assert.assertFalse(errors.isEmpty());
        Assert.assertEquals(2, errors.size());
        testGraphIntersection(errors.get(0), "[A, B]");
        testGraphIntersection(errors.get(1), "[C]");
    }

    @Test
    public void testInvalid() {
        GraphLanguageParser parser = new GraphLanguageParser();
        String graphDesc = "43675/()abc";
        parser.parse(graphDesc);
        List<ParseError> errors = parser.getErrors();
        Assert.assertFalse(errors.isEmpty());
        Assert.assertEquals(graphDesc.length() + 1, errors.size());
        for (int i = 0; i < graphDesc.length(); i++) {
            testRecognitionError(errors.get(i), i, graphDesc.charAt(i));
        }
    }

    @Test
    public void testVertexConnectedToSelf() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("ABBCDEF");
        List<ParseError> errors = parser.getErrors();
        Assert.assertFalse(errors.isEmpty());
        Assert.assertEquals(1, errors.size());
        Assert.assertEquals("Vertex connected to self: B", errors.get(0).getMessage());
    }

    @Test
    public void testVertexConnectedToSelfMulti() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("AABBCDEFF");
        List<ParseError> errors = parser.getErrors();
        Assert.assertFalse(errors.isEmpty());
        Assert.assertEquals(3, errors.size());
        testVertexConnectedToSelf(errors.get(0), 'A');
        testVertexConnectedToSelf(errors.get(1), 'B');
        testVertexConnectedToSelf(errors.get(2), 'F');
    }

    @Test
    public void testNullInput() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("");
        List<ParseError> errors = parser.getErrors();
        Assert.assertFalse(errors.isEmpty());
        Assert.assertEquals("No graph defined.", errors.get(0).getMessage());
        Assert.assertTrue(parser.getGraphs().isEmpty());
    }

    @Test
    public void testOneVertex() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("A");
        Assert.assertTrue(parser.getErrors().isEmpty());
        Assert.assertEquals(1, parser.getGraphs().size());
    }

    @Test
    public void testSameEdges() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("AB-C-BA");
        List<ParseError> errors = parser.getErrors();
        Assert.assertEquals(2, errors.size());
        testEdgeAlreadyExists(errors.get(0), "C-B");
        testEdgeAlreadyExists(errors.get(1), "B-A");
    }

    @Test
    public void testSimple() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("AB-C");
        List<ParseError> errors = parser.getErrors();
        Assert.assertTrue(errors.isEmpty());
        Assert.assertEquals(1, parser.getGraphs().size());
    }

    @Test
    public void testWhiteSpaceError() {
        GraphLanguageParser parser = new GraphLanguageParser();
        parser.parse("A B C");
        List<ParseError> errors = parser.getErrors();
        Assert.assertFalse(errors.isEmpty());
        Assert.assertEquals(2, errors.size());
        testRecognitionError(errors.get(0), 1, ' ');
        testRecognitionError(errors.get(1), 3, ' ');
    }

    private void testEdgeAlreadyExists(ParseError error, String edge) {
        Assert.assertEquals("Edge already exists: " + edge, error.getMessage());
    }

    private void testGraphIntersection(ParseError error, String vertices) {
        Assert.assertEquals("Vertex/vertices reused: " + vertices, error.getMessage());
    }

    private void testVertexConnectedToSelf(ParseError error, char vertex) {
        Assert.assertEquals("Vertex connected to self: " + vertex, error.getMessage());
    }

    private void testRecognitionError(ParseError error, int pos, char actual) {
        Assert.assertEquals(String.format("token recognition error at: '%s'", actual), error.getMessage());
        Assert.assertEquals(pos, error.getBegin());
    }
}
